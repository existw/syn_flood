#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <errno.h>
#include <netinet/tcp.h>
#include <netinet/ip.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#pragma pack(1)
struct pseudo_header    //needed for checksum calculation
{
  unsigned int source_address;
  unsigned int dest_address;
  unsigned char placeholder;
  unsigned char protocol;
  unsigned short tcp_length;

  struct tcphdr tcp;
};
#pragma pack()

unsigned short csum(unsigned short *ptr, int nbytes) {
  long sum;
  unsigned short oddbyte;
  short answer;


  sum = 0;
  while (nbytes > 1) {
    sum += *ptr++;
    nbytes -= 2;
  }
  if (nbytes == 1) {
    oddbyte = 0;
    *((u_char*) &oddbyte) = *(u_char*) ptr;
    sum += oddbyte;
  }

  sum = (sum >> 16) + (sum & 0xffff);
  sum = sum + (sum >> 16);
  answer = (short) ~sum;


  return (answer);
}

void oneSyn(int socketfd, in_addr_t source, u_int16_t sourcePort,
    in_addr_t destination, u_int16_t destinationPort) {
  static char sendBuf[sizeof(struct iphdr) + sizeof(struct tcphdr)] = { 0 };
  bzero(sendBuf, sizeof(sendBuf));

  struct iphdr* ipHeader = (struct iphdr*) sendBuf;
  struct tcphdr *tcph = (struct tcphdr*) (sendBuf + sizeof(struct iphdr));

  ipHeader->version = 4;
  ipHeader->ihl = 5;

  ipHeader->tos = 0;
  ipHeader->tot_len = htons(sizeof(sendBuf));

  ipHeader->id = htons(1);
  ipHeader->frag_off = 0;
  ipHeader->ttl = 254;
  ipHeader->protocol = IPPROTO_TCP;
  ipHeader->check = 0;
  ipHeader->saddr = source;
  ipHeader->daddr = destination;

  ipHeader->check = csum((unsigned short*) ipHeader, ipHeader->ihl * 2);

  //TCP Header
  tcph->source = htons(sourcePort);
  tcph->dest = htons(destinationPort);
  tcph->seq = 0;
  tcph->ack_seq = 0;
  tcph->doff = 5; //sizeof(tcphdr)/4
  tcph->fin = 0;
  tcph->syn = 1;
  tcph->rst = 0;
  tcph->psh = 0;
  tcph->ack = 0;
  tcph->urg = 0;
  tcph->window = htons(512);
  tcph->check = 0;
  tcph->urg_ptr = 0;

  //tcp header checksum
  struct pseudo_header pseudoHeader;
  pseudoHeader.source_address = source;
  pseudoHeader.dest_address = destination;
  pseudoHeader.placeholder = 0;
  pseudoHeader.protocol = IPPROTO_TCP;
  pseudoHeader.tcp_length = htons(sizeof(struct tcphdr));
  memcpy(&pseudoHeader.tcp, tcph, sizeof(struct tcphdr));

  tcph->check = csum((unsigned short*) &pseudoHeader, sizeof(struct pseudo_header));

  struct sockaddr_in sin;
  sin.sin_family = AF_INET;
  sin.sin_port = htons(sourcePort);
  sin.sin_addr.s_addr = destination;

  ssize_t sentLen = sendto(socketfd, sendBuf, sizeof(sendBuf), 0,
      (struct sockaddr *) &sin, sizeof(sin));
  if (sentLen == -1) {
    perror("sent error");
  }
}


#define USAGE "usage:\n"\
    "\t-S source address\n" \
    "\t-D destination address\n" \
    "\t-s source port start\n" \
    "\t-d destination port\n" \
    "\t-p max source port\n" \
    "\t-n packets per second, less than 1000\n\n" \
    "\tsudo ./a.out -S 192.168.110.200 -s 1234 -D 192.168.110.1 -d 80 -p 65535 -n 10\n\n"

int main(int argc, char **argv) {
  //for setsockopt
  int optval = 1;
  int param;
  char srcaddr[16] = {0};
  char dstaddr[16] = {0};
  u_int16_t sport = 2000;
  u_int16_t dport = 0;
  u_int16_t maxport = 65535;
  time_t timeout = 1000*1000; // one second

  while (-1 != (param=getopt(argc, argv, "S:s:D:d:p:n:"))) {
      switch (param) {
          case 'S':
          strcpy(srcaddr, optarg);
          break;
          case 's':
          sport = (u_int16_t)atoi(optarg);
          break;
          case 'D':
          strcpy(dstaddr, optarg);
          break;
          case 'd':
          dport = (u_int16_t)atoi(optarg);
          break;
          case 'p':
          maxport = (u_int16_t)atoi(optarg);
          break;
          case 'n':
          if (atoi(optarg)>1000) {
              printf("Per second %d packets.\n", (int)(atoi(optarg)%1000));
              sleep(2);
          }
          timeout = (time_t) (timeout / (float)(atoi(optarg)>1000? (atoi(optarg)%1000):atoi(optarg)));
          break;
          default:
          printf("invailied argment: %c\n", param);
          printf(USAGE);
          exit(0);
      }
  }

  if (strlen(srcaddr) <= 0
        || strlen(srcaddr) > 16
        || strlen(dstaddr) <= 0
        || strlen(dstaddr) > 16
        || sport < 1024) {
    printf("Fatal: invailied argment\n");
    printf(USAGE);
    exit(0);
  }

  //create a raw socket
  int socketfd = socket(PF_INET, SOCK_RAW, IPPROTO_TCP);
  if (socketfd == -1) {
    perror("create socket:");
    exit(0);
  }
  if (setsockopt(socketfd, IPPROTO_IP, IP_HDRINCL, &optval, sizeof(optval))
      < 0) {
    perror("create socket:");
    exit(0);
  }

  in_addr_t source = inet_addr(srcaddr);
  in_addr_t destination = inet_addr(dstaddr);
  u_int16_t sourcePort = sport;
  u_int16_t destinationPort = dport;
  while (1) {
      printf("tcp.srcport: %d\n", sourcePort);
      oneSyn(socketfd, source, sourcePort++, destination, destinationPort);
      if (! (sourcePort %= maxport)) {
          break;
      }
    usleep(timeout);
  }

  return 0;
}
